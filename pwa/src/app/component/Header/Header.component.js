/* eslint-disable max-len */

/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import SourceHeader from 'SourceComponent/Header/Header.component';
import ClickOutside from 'Component/ClickOutside';
import OfflineNotice from 'Component/OfflineNotice';
import isMobile from 'Util/Mobile';

export * from 'SourceComponent/Header/Header.component';

export default class Header extends SourceHeader {

    renderAccountButton(isVisible = false) {
        const {
            onMyAccountOutsideClick,
            onMyAccountButtonClick,
            isCheckout
        } = this.props;

        // on mobile and tablet hide button if not in checkout
        if ((isMobile.any() || isMobile.tablet()) && !isCheckout) {
            return null;
        }

        if (isCheckout && isSignedIn()) {
            return null;
        }

        return (
            <ClickOutside onClick={ onMyAccountOutsideClick } key="account">
                <div
                  aria-label="My account"
                  block="Header"
                  elem="MyAccount"
                >
                    <button
                      block="Header"
                      elem="MyAccountWrapper"
                      tabIndex="0"
                      onClick={ onMyAccountButtonClick }
                      aria-label="Open my account"
                      id="myAccount"
                    >
                        <div
                          block="Header"
                          elem="MyAccountTitle"
                        >
                            { __('Sign up') }
                        </div>
                        <div
                          block="Header"
                          elem="Button"
                          mods={ { isVisible, type: 'account' } }
                        />
                    </button>
                    { this.renderAccountOverlay() }
                </div>
            </ClickOutside>
        );
    }

    render() {
        const {
            navigationState: { name, isHiddenOnMobile = false },
            isCheckout
        } = this.props;

        return (
            <>
                <header block="Header" mods={ { name, isHiddenOnMobile, isCheckout } }>
                    {/* { this.renderTopMenu() } */}
                    <nav block="Header" elem="Nav">
                        { this.renderNavigationState() }
                    </nav>
                    { this.renderMenu() }
                </header>
                <OfflineNotice />
            </>
        );
    }
}
